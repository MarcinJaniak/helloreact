export const SAVE_FUEL_SAVINGS = 'SAVE_FUEL_SAVINGS';
export const CALCULATE_FUEL_SAVINGS = 'CALCULATE_FUEL_SAVINGS';

export const BANK_DEPOSIT = 'BankDeposit';
export const BANK_WITHDRAW = 'BankWithdraw';
