import {BANK_DEPOSIT} from "../constants/actionTypes";
import {BANK_WITHDRAW} from "../constants/actionTypes";



export const bankDeposit =(amount)=> {
  return {
    type: BANK_DEPOSIT,
    payload: {
      amount
    }
  };
};

export const bankWithdraw =(amount)=> {
  return {
    type: BANK_WITHDRAW,
    payload: {
      amount
    }
  };
};
