import { combineReducers } from 'redux';
import fuelSavings from './fuelSavingsReducer';
import { routerReducer } from 'react-router-redux';
import {bankReducer} from "./bankReducer";

const rootReducer = combineReducers({
  fuelSavings,
  routing: routerReducer,
  bank: bankReducer
});

export default rootReducer;
