import {BANK_DEPOSIT, BANK_WITHDRAW} from "../constants/actionTypes";

const initialState =
  {
balance:15000
  };

export const bankReducer = (state = initialState, action) =>
{
  if(action.type === BANK_DEPOSIT) {
    return {
      ...state,
      balance: state.balance + action.payload.amount
    };
  }
    if(action.type === BANK_WITHDRAW) {
      return {
        ...state,
        balance: state.balance - action.payload.amount
      };

  }
  return state;
};
