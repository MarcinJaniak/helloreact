import  React from 'react';
import  PropTypes from 'prop-types';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from "../actions/bankActions";

class BankPage extends  React.Component
{

  constructor()
  {
    super();
/*    this.deposit = this.deposit.bind(this);
    this.withdraw = this.withdraw.bind(this);*/
  }

  deposit()
  {
    this.props.bankDeposit(100);
  }

  withdraw()
  {
    this.props.bankWithdraw(100);
  }

  render() {
    const {balance = 20000, bankDeposit, bankWithdraw} = this.props;
    return (
      <div>
      <h1>{balance}</h1>
      <button onClick={()=>bankDeposit(100)}>Deposit</button>
        <button onClick={()=>bankWithdraw(100)}>Withdraw</button>
      </div>
    );
  }
}

BankPage.propTypes = {
  balance: PropTypes.number.isRequired,
  location: PropTypes.string,
  bankDeposit:PropTypes.func.isRequired,
  bankWithdraw:PropTypes.func.isRequired}
  ;

function mapStateToProps(state) {return {balance:state.bank.balance};}

function mapDispatchToProps(dispatch) {return bindActionCreators({bankDeposit: actions.bankDeposit, bankWithdraw: actions.bankWithdraw}, dispatch);}

export default connect(mapStateToProps, mapDispatchToProps)(BankPage);
